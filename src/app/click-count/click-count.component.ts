import { Component } from '@angular/core';

@Component({
  selector: 'app-click-count',
  templateUrl: './click-count.component.html'
})
export class ClickCountComponent {

  private clickCount = 0;

  increaseClickCount(event: MouseEvent) {
    event.stopPropagation();

    this.clickCount++;
  }

}
