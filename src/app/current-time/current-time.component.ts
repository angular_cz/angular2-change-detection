import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-current-time',
  template: '{{date.toLocaleTimeString()}}'
})
export class CurrentTimeComponent implements OnInit, OnDestroy {

  date = new Date();

  private intervalId: any;

  ngOnInit() {
    this.intervalId = setInterval(() => {
      console.log('updateTime');
      this.date = new Date();
    }, 1000);
  }

  ngOnDestroy(): void {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }


}
