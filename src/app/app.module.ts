import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ClickCountComponent } from './click-count/click-count.component';
import { ContainerComponent } from './container/container.component';
import { CurrentTimeComponent } from './current-time/current-time.component';
import { RenderInfoComponent } from './render-info/render-info.component';
import { ContainerDetachedComponent } from './container-detached/container-detached.component';

@NgModule({
  declarations: [
    AppComponent,
    ClickCountComponent,
    ContainerComponent,
    CurrentTimeComponent,
    RenderInfoComponent,
    ContainerDetachedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
