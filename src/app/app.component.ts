import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from "rxjs";
import 'rxjs/operator/map'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  titleObject = {
    title: 'Change detection example!'
  };

  timeFlag = false;

  componentFlags ={
    c1: true,
    c2: true,
    c3: true,
    c4: true
  };

  time$: Observable<Date>;

  ngOnInit() {
    this.time$ = Observable.interval(5000)
      .startWith(null)
      .map(() => new Date());
  }

  toggle() {
    this.timeFlag = !this.timeFlag;
  }

  resetTitleObject() {
    this.titleObject = {
      title: 'Reset title!'
    }
  }
}
