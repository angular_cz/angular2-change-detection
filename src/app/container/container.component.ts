import {
  Component, ChangeDetectionStrategy, Input, ChangeDetectorRef, OnInit, OnChanges, SimpleChanges, OnDestroy
} from '@angular/core';
import { Observable, Subscription } from "rxjs";

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  name = 'anonymous';

  @Input()
  text = '';

  @Input()
  textInObject = {text: ''};

  @Input()
  timeObservable: Observable<Date>;

  currentTime = new Date();

  checkCount_ = 0;

  private subscription: Subscription;

  get checkCount() {
    return this.checkCount_++;
  }

  constructor(protected changeDetectorRef: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes)
  }

  ngOnInit(): void {
    if (this.timeObservable) {
      this.subscription = this.timeObservable
        .subscribe((time) => {
          console.log('timeObservable');
          this.changeDetectorRef.markForCheck();
          this.currentTime = time
        });
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
