import {
  Component, ChangeDetectorRef, AfterViewInit, HostListener,
  ChangeDetectionStrategy
} from '@angular/core';
import { ContainerComponent } from "../container/container.component";


@Component({
  selector: 'app-container-detached',
  templateUrl: '../container/container.component.html',
  styleUrls: ['../container/container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerDetachedComponent extends ContainerComponent implements AfterViewInit {

  private live = false;

  ngAfterViewInit(): void {
    this.changeDetectorRef.detach();
  }

  constructor(changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  @HostListener('click')
  onClick() {
    if (this.live) {
      console.log('detach');
      this.changeDetectorRef.detach();
    } else {
      console.log('reattach');
      this.changeDetectorRef.reattach();
    }

    this.live = !this.live;

  }


}
