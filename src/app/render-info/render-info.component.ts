import { Component, NgZone } from '@angular/core';

@Component({
  selector: 'app-render-info',
  templateUrl: './render-info.component.html'
})
export class RenderInfoComponent {

  private checkCount_ = 0;

  constructor(private zone: NgZone) { }

  getCurrentDate() {
    return new Date();
  }

  get checkCount() {
    this.zone.runOutsideAngular(() =>   // run asynchronously outside change detection
      setTimeout(() => {this.checkCount_++}, 0)
    );
    return this.checkCount_;
  }

}
